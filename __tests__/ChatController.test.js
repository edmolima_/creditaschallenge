import sinon from 'sinon'
import ChatController from '../src/Chat/Chat.controller'

describe('#ChatController', () => {
  beforeEach(() => {
    document.body.innerHTML = '<div id="app"></div>'
  })
  test('should subscribeMessage called', () => {
    const controller = new ChatController('#app')
    const spy = sinon.spy(controller, 'subscribeMessage')

    controller.subscribeMessage()

    expect(spy.calledOnce).toBe(true)
  })

  test('should call controller initialization', () => {
    const controller = new ChatController('#app')
    const spy = sinon.spy(controller, 'initialize')

    controller.initialize()

    expect(spy.calledOnce).toBe(true)
  })
})
