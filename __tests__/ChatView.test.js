import sinon from 'sinon'
import ChatView from '../src/Chat/Chat.view'

describe('#ChatView', () => {
  beforeEach(() => {
    document.body.innerHTML = '<div id="app"></div>'
  })

  test('should render the chat', () => {
    const ChatViewClass = new ChatView('#app')
    ChatViewClass.init()
    const chat = document.querySelector('.chat')

    expect(chat).toMatchSnapshot()
  })

  test('should register the submit function', () => {
    const ChatViewClass = new ChatView('#app')
    const spy = sinon.spy(ChatViewClass, '_handlerUserChat')

    ChatViewClass._handlerUserChat()

    expect(spy.calledOnce).toBe(true)
  })

  test('should register the scroll event function', () => {
    const ChatViewClass = new ChatView('#app')
    const spy = sinon.spy(ChatViewClass, '_handlerScrollBottom')
    ChatViewClass.init()

    ChatViewClass._handlerScrollBottom()

    expect(spy.calledOnce).toBe(true)
  })
})
