import { Util } from '../src/Utils/Util'

describe('#Util', () => {
  test('should format the date in standard time', () => {
    const mockValue = new Date('2018-07-16T03:55:27.982Z')

    const wrapper = new Util()
    const tree = wrapper.formatDate(mockValue)

    expect(tree).toEqual('0h55')
  })
})
