import sinon from 'sinon'
import MessagingPattern from '../src/MessagingPattern'

describe('#MessagingPattern', () => {
  test('should called publish', () => {
    const PubSub = new MessagingPattern()
    const spy = sinon.spy(PubSub, 'publish')

    PubSub.publish()
    expect(spy.calledOnce).toBe(true)
  })

  test('should called subscribe', () => {
    const PubSub = new MessagingPattern()
    const spy = sinon.spy(PubSub, 'subscribe')

    PubSub.subscribe()
    expect(spy.calledOnce).toBe(true)
  })
})
