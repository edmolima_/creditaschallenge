import Message from '../src/Message'

describe('#Message', () => {
  test('should return message id 1', () => {
    const message = new Message('My message creditas')

    expect(message.getId()).toBe('1')
  })

  test('should return author Creditas', () => {
    const message = new Message('Message')
    expect(message.getAuthor()).toBe('Creditas')
  })

  test('should return message: My message creditas', () => {
    const message = new Message('My message creditas')

    expect(message.getMessage()).toBe('My message creditas')
  })

  test('should return date formated', () => {
    const message = new Message('My message creditas')

    expect(message.getDate()).toBe(message._formatDate(new Date()))
  })

  test('should return a message object', () => {
    const message = new Message('my message')

    expect(typeof message).toBe('object')
  })
})
