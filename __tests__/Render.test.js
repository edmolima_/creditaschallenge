import { Render } from '../src/Utils/Render'

describe('#Render', () => {
  beforeEach(() => {
    document.body.innerHTML = '<div id="app"></div>'
  })
  test('should render an element in the DOM', () => {
    const template = '<div id="template"></div>'
    const element = new Render()
    element.createElement('#app', template)

    expect(template).toMatchSnapshot()
  })
})
