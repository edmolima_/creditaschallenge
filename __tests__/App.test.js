import sinon from 'sinon'
import App from '../src/app'

describe('#App', () => {
  beforeEach(() => {
    document.body.innerHTML = '<div id="app"></div>'
  })

  test('should initialize is called', () => {
    const appWrapper = new App('#app')
    const spy = sinon.spy(appWrapper, 'initialize')

    appWrapper.initialize()

    expect(spy.calledOnce).toBe(true)
    expect(appWrapper).toMatchSnapshot()
  })
})
