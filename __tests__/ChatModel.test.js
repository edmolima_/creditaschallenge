import ChatModel from '../src/Chat/Chat.model'

describe('#ChatModel', () => {
  test('should create a message object', () => {
    const mockMessage = {
      id: '1',
      author: 'Edmo',
      date: 'hour',
      message: 'Hello'
    }

    const wrapper = new ChatModel()
    wrapper.create(mockMessage)

    expect(wrapper.messages).toEqual([mockMessage])
  })

  test('should return the message array', () => {
    const mockMessage = {
      id: '1',
      author: 'Edmo',
      date: 'hour',
      message: 'Hello'
    }

    const mockMessageTwo = {
      id: '2',
      author: 'Edmo 2',
      date: 'hour',
      message: 'Hello 2'
    }

    const wrapper = new ChatModel()

    wrapper.create(mockMessage)
    wrapper.create(mockMessageTwo)

    expect(wrapper.getMessages()).toEqual([mockMessage, mockMessageTwo])
  })
})
