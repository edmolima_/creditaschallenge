
# Descrição

Esse é um teste para a vaga de frontend na creditas, o mesmo tem como premissa a criação de um chat
que possa ser escalavél. Você pode ver aqui a descrição do desafio: [Challenge Creditas](https://github.com/Creditas/challenge/tree/master/frontend).

A aplicação foi desenvolvida com os pré-requisitos impostos na descrição do teste e a primeira consideração para solucionar o problema proposto é de que a programação funcional e reativa resolveria melhor esse tipo de problema já que prega a imutabilidade nos possibilitando receber todos os inputs do usuário de forma imutavel e temos a resiliência do reativo para podermos ter estratégias na hora das falhas do sistema. Há bastante ferramentais para dar apoio a isso principalmente no backend como o Elixir e o uso de RX para apoaiar-nos.

## Pré requisitos

- NodeJS 8.11LTS ou superior.

## Inicialização.

Para a inicialização é necessário instalar todas as dependências, siga o passo a passo para iniciar o projeto:

Instalar dependências:

```
npm run install
```

Inicializar projeto em modo de desenvolvimento:

```
npm run start
```

## Testes e Análise de Códico

Rodar testes unitários:

```
npm run test
```

Analisar coverage:

```
npm run test:coverage
```

Rodar verificador de padrão de código:

```
npm run lint
```

## Build

Fazer build para produção:

```
npm run build
```

# Versionamento

Para melhor gerênciamento da aplicação e manutenção foi escolhido o padrão de versionamento [Semantic Version](https://semver.org/)

# Observações

Utilizamos husky (hook git) configurado para pre-commit e pre-push. Na sua definição temos rodando o lint com configuração [standard](https://standardjs.com/) no commit, para seguirmos um padrão de código comum. Em seu pre-push temos os testes onde há uma configuração de coverage em 80% de cobertura, pois considero aceitavél.

# Tecnologias

 - ES6+
 - SCSS
 - Webpack
 - Jest
 - Eslint with Standard pattern
