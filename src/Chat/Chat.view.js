import { Render } from '../Utils/Render'
import MessagingPattern from '../MessagingPattern'

class ChatView extends Render {
  constructor (selector) {
    super()
    this.selector = selector
    this.render = this.createElement
    this.PubSub = new MessagingPattern()
    this.messageSelector = 'ul[data-chat="message-list"]'
  }

  init () {
    this.render(this.selector, this.template())
    this._handlerUserChat()
  }

  _handlerUserChat () {
    const ChatNode = document.querySelector(this.selector)

    ChatNode.addEventListener('submit', event => {
      event.preventDefault()

      const messageUser = ChatNode.querySelector(
        'input[data-chat="message-input"]'
      )

      if (messageUser.value.length > 0) {
        this.PubSub.publish('UserMessage', messageUser.value)

        messageUser.value = ''

        this._handlerScrollBottom()
      } else {
        alert('Ops! Você não digitou uma mensagem.') /* eslint-disable-line */
      }
    })
  }

  _handlerScrollBottom () {
    const MESSAGE_LIST = document.querySelector(this.messageSelector)
    return (MESSAGE_LIST.scrollTop = MESSAGE_LIST.scrollHeight)
  }

  renderMessage (message) {
    const template = `
      <li class="chat-message__item" id=${message.id}>
        <p class="chat-message__text">
          ${message.message}
        </p>
        <div class="chat-message__user">
          <p>Analista: <strong>${message.author} - ${message.date}</strong></p>
        </div>
      </li>
    `

    this.render(this.messageSelector, template)
  }

  template () {
    return `
      <section class="chat">
        <h1 class="chat-title">Chat</h1>
        <section class="chat-wrapper">
          <ul class="chat-message__list" data-chat="message-list">
          </ul>
          <form class="chat-message__wrapper" >
            <input type="text" class="chat-message__input" data-chat="message-input" placeholder="Digite sua mensagem..." required />
            <button type="submit" class="button chat-message__button">Enviar</button>
          </form>
        </section>
      </section>
    `
  }
}

export default ChatView
