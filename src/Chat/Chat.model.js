class ChatModel {
  constructor () {
    this.messages = []
  }

  getMessages () {
    return this.messages
  }

  create (message) {
    if (message.id && message.message && message.date && message.author) {
      this.messages.push(message)
    } else {
      throw new Error('There was some problem with the arguments')
    }
  }
}

export default ChatModel
