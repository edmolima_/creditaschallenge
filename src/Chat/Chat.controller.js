import ChatModel from './Chat.model'
import ChatView from './Chat.view'
import Message from '../Message'

import MessagingPattern from '../MessagingPattern'

class ChatController {
  constructor (selector) {
    this.PubSub = new MessagingPattern()
    this.ChatModel = new ChatModel()
    this.ChatView = new ChatView(selector)
  }

  initialize () {
    this.ChatView.init()
    this.subscribeMessage()
  }

  subscribeMessage () {
    this.PubSub.subscribe('UserMessage', (msg, UserMessage) => {
      const message = new Message(UserMessage)
      this.ChatModel.create(message)

      this.ChatView.renderMessage(message)
    })
  }
}

export default ChatController
