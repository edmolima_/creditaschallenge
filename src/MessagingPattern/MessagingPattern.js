import PubSub from 'pubsub-js'

class MessagingPattern {
  constructor () {
    this.PubSub = PubSub
  }

  publish (topic, message) {
    this.PubSub.publish(topic, message)
  }

  subscribe (topic, callback) {
    this.PubSub.subscribe(topic, callback)
  }
}

export default MessagingPattern
