export class Render {
  createElement (selector, template) {
    const node = document.querySelector(selector)
    node.innerHTML += template
  }
}
