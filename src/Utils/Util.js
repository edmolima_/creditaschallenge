export class Util {
  formatDate (date) {
    const time = `${date.getHours()}h${
      date.getMinutes() < 10 ? '0' : ''
    }${date.getMinutes()}`

    return time
  }
}
