import ChatController from './Chat/Chat.controller'

export default class App {
  constructor (selector) {
    this.selector = selector
  }

  initialize () {
    document.addEventListener('DOMContentLoaded', () => {
      const Chat = new ChatController(this.selector)
      Chat.initialize()
    })
  }
}
