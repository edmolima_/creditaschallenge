import { Util } from '../Utils/Util'
import uniqueId from 'lodash.uniqueid'

const USER_DEFAULT = 'Creditas'

class Message {
  constructor (message) {
    this.id = uniqueId()
    this.author = USER_DEFAULT
    this.date = this._formatDate(new Date())
    this.message = message
  }

  _formatDate (date) {
    const format = new Util()
    return format.formatDate(date)
  }

  getId () {
    return this.id
  }

  getMessage () {
    return this.message
  }

  getAuthor () {
    return this.author
  }

  getDate () {
    return this.date
  }
}

export default Message
